import numpy as np

import tflite_runtime.interpreter as tflite # Tensorflow Lite
# import tensorflow as tf # Tensorflow 

X_test = np.load('./mne_sample/X_test.npy')
Y_test = np.load('./mne_sample/Y_test.npy')

# Load the TFLite model and allocate tensors.
interpreter = tflite.Interpreter(model_path="model.tflite") # Tensorflow Lite
# interpreter = tf.lite.Interpreter(model_path="model.tflite") # Tensorflow 
interpreter.allocate_tensors()

# Get input and output tensors.
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()


output_data_all = np.array([0,0,0,0])
for i in range(0, 72): 
    input_data = np.float32(X_test[i:i+1,:,:,:])
    interpreter.set_tensor(input_details[0]['index'], input_data)

    interpreter.invoke()

    # The function `get_tensor()` returns a copy of the tensor data.
    # Use `tensor()` in order to get a pointer to the tensor.
    output_data = interpreter.get_tensor(output_details[0]['index'])
    output_data_all = np.vstack([output_data_all, output_data[0]])
    
output_data_all = output_data_all[1:]
preds       = output_data_all.argmax(axis = -1) # argmax - return the indices of the maximum values
acc         = np.mean(preds == Y_test.argmax(axis=-1))
print("\nClassification tflite accuracy: %f \n" % (acc))



